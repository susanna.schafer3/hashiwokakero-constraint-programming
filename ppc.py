# -*- coding: utf-8 -*-

from docplex.cp.model import *
from docplex.cp.config import get_default
from igraph import *

#grid = [[3, 0, 4, 1], [4, 2, 0, 0], [0, 0, 2, 0], [4, 5, 0, 3]]
#grid = [[2, 2]]
#grid = [[2, 3, 0, 4, 0, 2, 0], 
        #[0, 0, 0, 0, 0, 0, 2],
        #[1, 1, 0, 0, 1, 3, 3],
        #[2, 0, 0, 8, 0, 5, 2],
        #[3, 0, 3, 0, 0, 0, 1],
        #[0, 0, 2, 0, 0, 3, 4],
        #[3, 0, 0, 3, 1, 0, 2]]
#grid = [[2, 3, 0, 4, 0, 2, 0],
 #       [0, 0, 0, 0, 0, 0, 2],
  #      [1, 1, 0, 0, 1, 3, 3],
   #     [2, 0, 0, 8, 0, 5, 2],
    #    [3, 0, 3, 0, 0, 0, 1],
     #   [0, 0, 2, 0, 0, 3, 4],
      #  [3, 0, 0, 3, 1, 0, 2]]
grid = [[2, 0, 1],
        [2, 0, 1]]
#grid = [[3, 0, 2]]
model = CpoModel()
islands = []
islandsI = []
islandsJ = []
nbColumns = len(grid[0])
nbLines = len(grid)
for lineIdx, line in enumerate(grid):
    for squareIdx, square in enumerate(line):
        if square:
            islands.append(square)
            islandsI.append(lineIdx)
            islandsJ.append(squareIdx)
nbIslands = len(islands)
possibleLinks = [[0 for i in range(nbIslands)] for j in range(nbIslands)]
liens = model.integer_var_list(nbIslands*nbIslands, min=0, max=2, name='x')
for islandIdx, island in enumerate(islands):
    if(islandIdx < (nbIslands-1)):
        #une ile ne peut pas etre reliee a elle meme
        model.add(liens[islandIdx*nbIslands+islandIdx] == 0)
        horizontalOk = 0
        verticalOk = 0
        #On teste avec quelles iles on peut connecter l'ile en cours d'analyse (liens impossibles == 0)
        for island2Idx, island2 in enumerate(islands):
            if(island2Idx > islandIdx):
                #cohérence
                model.add(liens[islandIdx*nbIslands+island2Idx] == liens[island2Idx*nbIslands+islandIdx])
                #si lien horizontal possible
                if((not horizontalOk) and (islandsI[island2Idx] == islandsI[islandIdx])):
                    horizontalOk = 1
                    possibleLinks[islandIdx][island2Idx] = 1
                    possibleLinks[island2Idx][islandIdx] = 1
                #si lien vertical possible
                elif((not verticalOk) and (islandsJ[islandIdx] == islandsJ[island2Idx])):
                    verticalOk = 1
                    possibleLinks[islandIdx][island2Idx] = 1
                    possibleLinks[island2Idx][islandIdx] = 1
                #si lien impossible
                else:
                    model.add(liens[islandIdx*nbIslands+island2Idx] == 0)
                    model.add(liens[island2Idx*nbIslands+islandIdx] == 0)
        else:
            model.add(liens[(nbIslands-1)*nbIslands+nbIslands-1] == 0)
#somme des liens = valeur ile
for islandIdx, island in enumerate(islands):
    linkCountHorizontal = 0
    linkCountVertical = 0
    for index in range(nbIslands):
        linkCountHorizontal+=liens[islandIdx*nbIslands+index]
        linkCountVertical+=liens[index*nbIslands+islandIdx]
    model.add(linkCountHorizontal == island)
    model.add(linkCountVertical == island)
#2 liens ne doivent pas se croiser
for lineIdx, line in enumerate(possibleLinks):
    #impossible qu'un lien sur la première ou dernière ligne en croise un autre
        for possibleLinkIdx, possibleLink in enumerate(line):
            #pareil pour un lien sur première ou dernière colonne
                if(possibleLink == 1):
                    for line2Idx, line2 in enumerate(possibleLinks):
                        #pareil
                        if(line2Idx > lineIdx):
                            for possibleLink2Idx, possibleLink2 in enumerate(line2):
                                if(possibleLink2 == 1):
                                    #pareil
                                    #si premier lien vertical et deuxième horizontal
                                    if(islandsJ[lineIdx] == islandsJ[possibleLinkIdx] and islandsI[line2Idx] == islandsI[possibleLink2Idx]):
                                        #si deux dernières iles entre lignes des deux premières
                                        if((islandsI[lineIdx] < islandsI[line2Idx]) and (islandsI[possibleLinkIdx] > islandsI[line2Idx])):
                                        #pareil avec colonnes
                                            if(((islandsJ[line2Idx] < islandsJ[lineIdx]) and (islandsJ[possibleLink2Idx] > islandsJ[lineIdx])) or ((islandsJ[line2Idx] > islandsJ[lineIdx]) and (islandsJ[possibleLink2Idx] < islandsJ[lineIdx]))):
                                                model.add(if_then(liens[lineIdx*nbIslands+possibleLinkIdx] > 0, liens[line2Idx*nbIslands+possibleLink2Idx] == 0))
                                                model.add(if_then(liens[line2Idx*nbIslands+possibleLink2Idx] > 0, liens[lineIdx*nbIslands+possibleLinkIdx] == 0))
solutions = model.start_search()
solutionMatrix = []
for sol in solutions:
    graph = Graph()
    for idx in range(nbIslands):
        graph.add_vertex(f'{idx}')
    solutionMatrix = []
    for j in range(nbIslands*nbIslands):
        if(j%nbIslands == 0):
            solutionMatrix.append([])
        solutionMatrix[len(solutionMatrix)-1].append(sol[f'x_{j}'])
    for lineIdx, line in enumerate(solutionMatrix):
        if(lineIdx < nbIslands):
            for columnIdx, column in enumerate(line):
                if(column and columnIdx > lineIdx):
                    graph.add_edge(f'{lineIdx}', f'{columnIdx}')
    if(graph.is_connected()):
        sol.write()
        break
print(solutionMatrix)
